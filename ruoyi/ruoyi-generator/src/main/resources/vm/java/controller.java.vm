package ${packageName}.controller;

#if(${swagger})
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
#end
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
#if($table.crud || $table.sub)
import com.ruoyi.common.core.page.TableDataInfo;
#elseif($table.tree)
#end
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import ${packageName}.domain.${ClassName};
import ${packageName}.service.I${ClassName}Service;
#if(${swagger})
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
#end
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * ${functionName}Controller
 * 
 * @author ${author}
 * @date ${datetime}
 */
#if(${swagger})
@ApiSort(value = ${randomNo})
@Api(tags = "${functionName}控制器")
#end
@RestController
@RequestMapping("/${moduleName}/${businessName}")
public class ${ClassName}Controller extends BaseController {

    @Resource
    I${ClassName}Service ${className}Service;

    /**
     * 查询${functionName}列表
     * @param ${className} ${functionName}对象
     * @return ${functionName}分页数据
     */
#if(${swagger})
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "查询${functionName}列表")
    @ApiImplicitParam(name = "${className}", value = "${functionName}对象", paramType = "query", dataTypeClass = ${ClassName}.class)
#end
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:list')")
    @GetMapping(value = "/list")
#if($table.crud || $table.sub)
    public TableDataInfo<List<${ClassName}>> list(${ClassName} ${className}) {
        startPage();
        return getDataTable(this.${className}Service.select${ClassName}List(${className}));
    }
#elseif($table.tree)
    public AjaxResultList<${ClassName}> list(${ClassName} ${className}) {
        List<${ClassName}> list = this.${className}Service.select${ClassName}List(${className});
        return AjaxResult.success(list);
    }
#end

#if(${excelExport} && !${swagger})
    /**
     * 导出${functionName}列表
     *
     * @param ${className} ${functionName}对象
     * @return ${functionName}列表
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @GetMapping(value = "/export")
    public AjaxResult<String> export(${ClassName} ${className}) {
        List<${ClassName}> list = this.${className}Service.select${ClassName}List(${className});
        ExcelUtil<${ClassName}> util = new ExcelUtil<>(${ClassName}.class);
        return util.exportExcel(list, "${businessName}");
    }
#end
#if(${excelExport} && ${swagger})
    /**
     * 导出${functionName}列表
     *
     * @param ${className} ${functionName}对象
     * @return ${functionName}列表
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "导出${functionName}列表")
    @ApiImplicitParam(name = "${className}", value = "${functionName}对象", paramType = "query", dataTypeClass = ${ClassName}.class)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @GetMapping(value = "/export")
    public AjaxResult<String> export(${ClassName} ${className}) {
        List<${ClassName}> list = this.${className}Service.select${ClassName}List(${className});
        ExcelUtil<${ClassName}> util = new ExcelUtil<>(${ClassName}.class);
        return util.exportExcel(list, "${businessName}");
    }
#end

#if(${excelExport} && ${swagger})
    /**
    * 导出${functionName}列表(返回流)
    *
    * @param ${className} ${functionName}对象
    * @param response  返回
    *
    */
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "导出${functionName}列表(返回流)")
    @ApiImplicitParam(name = "${className}", value = "${functionName}对象", paramType = "query", dataTypeClass = ${ClassName}.class)
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @GetMapping(value = "/exportByStream", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void exportByStream(${ClassName} ${className}, HttpServletResponse response) {
        new ExcelUtil<>(${ClassName}.class).exportExcel(response, this.${className}Service.select${ClassName}List(${className}), "${functionName}");
    }
#end
#if(${excelExport} && !${swagger})
    /**
     * 导出${functionName}列表(返回流)
     *
     * @param ${className} ${functionName}对象
     * @param response  返回
     *
     */
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:export')")
    @Log(title = "${functionName}", businessType = BusinessType.EXPORT)
    @GetMapping(value = "/exportByStream", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void exportByStream(${ClassName} ${className}, HttpServletResponse response) {
        new ExcelUtil<>(${ClassName}.class).exportExcel(response, this.${className}Service.select${ClassName}List(${className}), "${functionName}");
    }
#end

    /**
     * 获取${functionName}详细信息
     *
     * @param ${pkColumn.javaField} ${functionName}主键
     */
#if(${swagger})
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "获取${functionName}详细信息")
    @ApiImplicitParam(name = "${pkColumn.javaField}", value = "${functionName}主键", paramType = "path", dataTypeClass = ${pkColumn.javaType}.class, required = true)
#end
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:query')")
    @GetMapping(value = "/{${pkColumn.javaField}}")
    public AjaxResult<String> getInfo(@PathVariable("${pkColumn.javaField}") ${pkColumn.javaType} ${pkColumn.javaField}) {
        return AjaxResult.success(this.${className}Service.select${ClassName}ById(${pkColumn.javaField}));
    }

    /**
     * 新增${functionName}
     *
     *  @param ${className} ${functionName}对象
     */
#if(${swagger})
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "新增${functionName}")
#end
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:add')")
    @Log(title = "${functionName}", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    public AjaxResult<String> add(@RequestBody ${ClassName} ${className}) {
        return toAjax(this.${className}Service.insert${ClassName}(${className}));
    }

    /**
     * 修改${functionName}
     *
     * @param  ${className} ${functionName}对象
     */
#if(${swagger})
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "修改${functionName}")
#end
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:edit')")
    @Log(title = "${functionName}", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/edit")
    public AjaxResult<String> edit(@RequestBody ${ClassName} ${className}) {
        return toAjax(this.${className}Service.update${ClassName}(${className}));
    }

    /**
     * 删除${functionName}
     *
     * @param ${pkColumn.javaField}s
     */
#if(${swagger})
    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "删除${functionName}")
    @ApiImplicitParam(name = "${pkColumn.javaField}s", value = "${functionName}主键数组", paramType = "path", dataTypeClass = ${pkColumn.javaType}.class, allowMultiple = true, required = true)
#end
    @PreAuthorize("@ss.hasPermi('${permissionPrefix}:remove')")
    @Log(title = "${functionName}", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{${pkColumn.javaField}s}")
    public AjaxResult<String> remove(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        return toAjax(this.${className}Service.delete${ClassName}ByIds(${pkColumn.javaField}s));
    }

}
